# Git Demo and Command References

## Getting Started

We will be following a Git Workflow similar to the Feature Branch Workflow which you can read more about in this [article](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow). The workflow involves the following main types of branhches: 

* **Main Branch** | Central branch that contains the latest bug-free version of your software.
* **Develop Branch** | Central branch that contains the working version of your software.
* **Feature Branches** | Contains the feature that you are currently working on.

## General Guidelines

Here are some general guidelines that you should follow while under a feature branch workflow. Success in following these guidelines will help minimize merge conflicts and tears of programming dread.

* You will almost never work directly on the main branch. Whenever you want to write/edit code, always start with creating a feature branch. 
* Feature branch should be short but describe the funcationality that you are adding. If you find that you can't think of a feature name for your functionality, that is a sign that you should split your functionality to smaller, simpler features. Your feature name should be all lower-case and separated by `-`.
    * Example Feature Names:
        - `login-form`
        - `signup-endpoint`
        - `update-readme`
        - `data-pull-navigation`
    * Counter Examples:
        - `Dr.R_Auth`
        - `lauren.signinuser-version2`
        - `infection-detection-app`


* Commit frequently and use relatively detailed commit message to describe your code. 
    * Example: "Created a text field component and added basic styling"
    * Counter Examples: "login page", "wrote some code hehe haha yeah!"
* Always announce in your subteam whenever a pull request of your feature branch is merged into `main` so that everyone can rebase their feature branches. 

## Feature Branch Workflow

### Clone the Project

1. Navigate to the directory where you want to store your project. 
2. In that directory, clone the repository with `git clone <project-url>`.
3. Follow setup instruction on that specific project's README.

### Development
1. (Situational) If your team member recently merged a pull request to the main branch, run `git pull` to make sure you have the latest update.
2. Create your feature branch with `git branch <feature-name>` 
3. Checkout to your feature branch with `git checkout <feature-name>`
4. Once you finish a modular section of your code (an endpoint, a component, a view, a function, etc.), add your files to the staging area with `git add <filename>`
5. Commit your changes with `git commit -m "<commit message>"`
6. After every working session, push your code to GitLab from local with `git push`
7. Once you are fully compelted with your feature branch, start a merge request in GitLab and select your reviewer. 

### Rebasing
Whenever a feature branch from your teammate was merged into main through a pull request, you will have to rebase the feature brances you are currently working on. 

<p align="center">
<img src="https://www.blog.duomly.com/wp-content/uploads/2020/05/Rebase.png" width="200px"></img>
</p>

1. To perform rebase, run the following command on your feature branch `git pull --rebase origin main`.
2. If there are merge conflicts, resolve them and then re-stage your files with `git add <filename>`.
3. Run `git rebase --continue` to complete rebase (Note: Do NOT run `git commit` during rebasing).
4. When rebase is completed, run `git push --force-with-lease` to push your local branch to remote. (Note: NEVER run `git push --force` or `git push --force-with-lease` on the main branch)

### Merge Conflict

The easiest way to deal with merge conflicts are not to create them. This means communicating well with your fellow teammates so that you are not editing the same scope of program in your respective feature branches. Resolving merge conflicts are typically very situational, so if you encounter a situation not described below, please contact us!

1. If you encounter a merge conflict when merging code from remote to local, resolve the merge conflict manually in your IDE and run `git add <filename>` followed by `git commit -m "<commit-message>"` to register the resolved merge conflict. 
2. If you encounter merge conflict when performing a merge request on the GitLab GUI, resolve it in the interactive window and continue with merge request as prompted. 

If you are unsure of how code should be merged, please don't hesistate to contact the admin team to get suggestions. 
